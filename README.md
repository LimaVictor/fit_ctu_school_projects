# FIT CTU School projects

Welcome to the repository with my school projects! Here you will find various assignments, lab works, and projects completed as part of my academic course.

## Project Structure

- **/BI-DBS/**: Database Systems. Semester task with a hotel database.
- **/BI-SQL/**: SQL Advanced. Subject dedicated to SQL optimization and additional language extensions.
- **/BI-PSI/**: Computer networks. Semester project. Server.
- **/BI-APS/**: Architectures of Computer Systems. Semester project. Description of processor operation in Verilog language.

## Programming Languages

Various technologies are used in my projects, including:
- Python
- SQL
- PL/SQL, PL/pgSQL

## About the Author

This repository is created to store my academic projects and reports on them.
