-- Dotaz: Muzska rodova linie vydry cislo 10 (10, Muzska)
WITH
rodova_linie(depth, JMENO, CV, OTEC, MATKA, pohlavi) AS   -- kotva;
 (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
  FROM vydra E1
  WHERE E1.cv = 10
  UNION ALL
  SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivní část dotazu
  FROM vydra E2 INNER JOIN rodova_linie ML ON (ML.Otec = E2.cv)
 )
SELECT rodova_linie
FROM
    (SELECT DISTINCT JMENO || ' (' || depth || ')' AS rodova_linie, depth, pohlavi -- DISTINCT pro vystup bez duplicit
     FROM rodova_linie
     ORDER BY depth)

-- Dotaz: Zenska rodova linie vydry cislo 10 (10, Zenska)
WITH
rodova_linie(depth, JMENO, CV, OTEC, MATKA, pohlavi) AS   -- kotva;
 (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
  FROM vydra E1
  WHERE E1.cv = 10
  UNION ALL
  SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivní část dotazu
  FROM vydra E2 INNER JOIN rodova_linie ML ON (ML.MATKA = E2.cv)
 )
SELECT rodova_linie
FROM
    (SELECT DISTINCT JMENO || ' (' || depth || ')' AS rodova_linie, depth, pohlavi -- DISTINCT pro vystup bez duplicit
     FROM rodova_linie
     ORDER BY depth)

-- Dotaz: Rodova linie vydry cislo 10 (10, Obe)
WITH
rodova_linie(depth, JMENO, CV, OTEC, MATKA, pohlavi) AS   -- kotva;
 (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
  FROM vydra E1
  WHERE E1.cv = 10
  UNION ALL
  SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivní část dotazu
  FROM vydra E2 INNER JOIN rodova_linie ML ON ((ML.MATKA = E2.cv) OR (ML.Otec = E2.cv))
 )
SELECT rodova_linie
FROM
    (SELECT DISTINCT JMENO || ' (' || depth || ')' AS rodova_linie, depth, pohlavi -- DISTINCT pro vystup bez duplicit
     FROM rodova_linie
     ORDER BY depth)
