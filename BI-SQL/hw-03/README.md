# uloha 03  - rekurzivní dotazování a procedura

## Cíle
  - vyzkoušet si rekurzivní dotazování
  - výsledek "zabalit" do procedury jako parametrizovaný kurzor a otestovat

## Zadání
  - tabulka vydry obsahuje rekurzivní data: sloupce matka, otec jsou cizí klíče odkazující zpět do tabulky vyder na cv (číslo vydry)
  - navrhněte rekurzivní SQL dotaz, který zobrazí rodovou linii předků dané vydry
  - v první iteraci si dotaz zjednodušte na mužskou nebo ženskou rodovou linii
  - ve druhé iteraci zkuste sledovat všechny předky a ve výpisu označte generace (můžete i nějak vhodně odsadit, aby to bylo vizuálně pěkné)
  - odladěný rekurzivní dotaz "zabalte" do procedury coby **kurzor s parametry** a paramterizujte ho číslem vydry a typem linie předků (mužská, ženská, obě)
  - procedura dostane jako parametr **jméno vydry** a linii po které se má vydat, proveďte standardní ošetření chybného vstupu a vhodným způsobem vypište výsledek. Procedura bude vracet jména vyder.

## Očekávané výstupy
 - soubor dotazy.sql - zde musíte použít pevné číslo vydry a také konkrétní typ rodové linie
   použijte tyto tři: (10, mužská), (10, ženská), (10, obě)
 - soubor procedura.sql, kde bude požadovaná procedura s parametrizovaným kurzorem

