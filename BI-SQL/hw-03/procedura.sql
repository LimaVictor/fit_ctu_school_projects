CREATE OR REPLACE PROCEDURE Rodokmen ( jmeno_in IN varchar2, linie_in IN varchar2)
    AUTHID definer
    IS
    zero_rows EXCEPTION;
    PRAGMA EXCEPTION_INIT(zero_rows, -20001);
    vydra_id NUMBER := NULL;
    v_name varchar2(20);
    CURSOR kmen (p_vydra NUMBER, p_linie IN VARCHAR2) IS
        WITH
        rodova_linie(depth, jmeno, CV, OTEC, MATKA, pohlavi) AS   -- kotva rekurze (uchycení)
         (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
          FROM vydra E1
          WHERE E1.cv = p_vydra
          UNION ALL
          SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivní část dotazu
          FROM vydra E2 INNER JOIN rodova_linie ML ON
          (p_linie = 'ženská' AND (ML.MATKA = E2.cv)) OR
          (p_linie = 'mužská' AND (ML.OTEC = E2.cv)) OR
          (p_linie = 'obě' AND ((ML.Otec = E2.cv) OR (ML.MATKA = E2.cv)))
         )
        SELECT TMP
        FROM
            (SELECT DISTINCT JMENO || ' (' || depth || ')' AS TMP, depth -- DISTINCT odstraneni duplicit
             FROM rodova_linie
             ORDER BY depth)
        WHERE depth > 0;    -- pouzito, aby ve vystupu se ne objevil radek o hledane vydre
BEGIN
    -- SELECTem Zjistim ID vydry (vydra_id) podle jmena (linie_in), ktere dostane procedura
    -- ID vydry predam kurzuru jako parametr (vydra_id)
    SELECT CV INTO vydra_id FROM VYDRA WHERE JMENO = jmeno_in;
    dbms_output.put_line(  INITCAP(linie_in) ||' rodová(e) linie vydry ' || jmeno_in || ' je:');
    OPEN kmen(vydra_id, linie_in);
    LOOP
        FETCH kmen INTO v_name;
        EXIT WHEN kmen%notfound;
        dbms_output.put_line(v_name);
    END LOOP;
    -- Kdyz nebude nalezena zadna informace o predcich, tak vyvolam vyjimku
    IF kmen%ROWCOUNT = 0 THEN
        RAISE zero_rows;
    END IF;
    CLOSE kmen;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE (' Zaznam o vydře jmenem ' || jmeno_in || ' neni v databazi. (Nezname jmeno vydry)');
    WHEN zero_rows THEN
    DBMS_OUTPUT.PUT_LINE ('Informace o předcích vydry ' || jmeno_in || ' nebyla nalezena');
END;