WITH RECURSIVE
rodova_linie(depth, JMENO, CV, OTEC, MATKA, pohlavi) AS   -- kotva;
 (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
  FROM vydra E1
  WHERE E1.cv = 10
  UNION ALL
  SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivni cast dotazu
  FROM vydra E2 INNER JOIN rodova_linie ML ON (ML.Otec = E2.cv)
 )
SELECT rodova_linie
FROM
    (SELECT concat(JMENO, ' (', depth,')') AS rodova_linie, depth, pohlavi -- DISTINCT pro vystup bez duplicit
     FROM rodova_linie
     ORDER BY depth) AS tmp