CREATE OR REPLACE PROCEDURE Rodokmen ( jmeno_in varchar, linie_in varchar) AS $$
  DECLARE
    vydra_id integer := NULL;
    v_name varchar(20);
	v_RowCountInt  integer;
	v_cnt int := 0;
    kmen CURSOR (p_vydra integer, p_linie varchar) FOR 
        WITH RECURSIVE
        rodova_linie(depth, jmeno, CV, OTEC, MATKA, pohlavi) AS   -- kotva rekurze (uchyceni)
         (SELECT 0, jmeno, CV, OTEC, MATKA, pohlavi
          FROM vydra E1
          WHERE E1.cv = p_vydra
          UNION ALL
          SELECT ML.depth + 1, E2.jmeno, E2.CV, E2.OTEC, E2.MATKA, E2.pohlavi  -- rekurzivni cast dotazu
          FROM vydra E2 INNER JOIN rodova_linie ML ON
          (p_linie = 'zenska' AND (ML.MATKA = E2.cv)) OR
          (p_linie = 'muzska' AND (ML.OTEC = E2.cv)) OR
          (p_linie = 'obe' AND ((ML.Otec = E2.cv) OR (ML.MATKA = E2.cv)))
         )
        SELECT TMP
        FROM
			(SELECT DISTINCT concat(JMENO, ' (', depth,')') AS TMP, depth, pohlavi -- DISTINCT pro vystup bez duplicit
             FROM rodova_linie
             ORDER BY depth) tmp2
        WHERE depth > 0;    -- pouzito, aby ve vystupu se ne objevil radek o hledane vydre
BEGIN
    -- SELECTem Zjistim ID vydry (vydra_id) podle jmena (linie_in), ktere dostane procedura
    -- ID vydry predam kurzuru jako parametr (vydra_id)
    SELECT CV INTO STRICT vydra_id FROM VYDRA WHERE JMENO = jmeno_in;
	RAISE NOTICE '% rodova(e) linie vydry % je:', linie_in, jmeno_in;
    OPEN kmen(vydra_id, linie_in);
    LOOP
        FETCH kmen INTO v_name;
		-- do promnene v_RowCountInt se zapise 1 v pripade, ze kurzor vrati radek. 
		GET DIAGNOSTICS v_RowCountInt := ROW_COUNT; v_cnt := v_cnt + v_RowCountInt;
		IF v_cnt = 0 THEN
		RAISE NOTICE 'Informace o predcich vydry % nebyla nalezena', jmeno_in;
    	END IF;
		IF NOT FOUND THEN EXIT; END IF;
		RAISE NOTICE '%',v_name;
    END LOOP;
    CLOSE kmen;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	RAISE NOTICE 'Zaznam o vydre jmenem % neni v databazi. (Nezname jmeno vydry)',jmeno_in;
END $$
LANGUAGE plpgsql;
