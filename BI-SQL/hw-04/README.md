# Úloha 04 - Optimalizace SQL

## Zadání:
- Zvolit si nějaký (netriviální) dotaz nad vaší databází z předmětu BI-DBS.
- Zobrazit a analyzovat prováděcí plán dotazu.
- Navrhnout změny, které povedou k efektivnějšímu vyhodnocení.
- Změny implementovat.
- Zobrazit a analyzovat prováděcí plán po implementaci změn.
- Zhodnotit provedenou optimalizaci.
- Postup opakovat pro další dva SQL dotazy.

Řešení je popsáno v souboru `Dokumentace_lubinval.pdf`.
