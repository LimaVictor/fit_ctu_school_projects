# BI-SQL Jazyk SQL, pokročilý

## Anotace

V tomto předmětu jsme se zabývali pokročilými relačními a nadrelačními rysy jazyka SQL, jako jsou procedury, funkce a package. Také jsme se zabývali rekurzivními dotazy a optimalizací provádění příkazů SQL.

## Použité technologie

Various technologies are used in my projects, including:
- `Oracle SQL`
- `Oracle PL/SQL`
- `PostgreSQL`
- `PL/pgSQL`
