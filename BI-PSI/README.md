# Python Robot Server

This Python script implements a server for controlling a robot's movement. It communicates with a client over a socket connection and processes commands for the robot's movement.

## Prerequisites

- Python 3.x

## Usage

1. Ensure you have Python installed.
2. Run the script providing a port number as a command-line argument.

Example:
```bash
python3 server.py <port_number>
```

## Commands

The server expects commands from the client in a specific format. The following commands are supported:

- `MOVE`: Move the robot.
- `TURN LEFT`: Turn the robot left.
- `TURN RIGHT`: Turn the robot right.
- `GET MESSAGE`: Retrieve a message.
- `LOGOUT`: Logout the client.

## Error Handling

The server handles various error conditions, including syntax errors, logic errors, and key out-of-range errors.

## Implementation Details

- The script uses socket programming to establish communication with the client.
- It implements functions for parsing commands, handling movement, and managing socket connections.
- Error handling is integrated throughout the code to ensure robustness.
