#!/usr/bin/env python3

from datetime import date
import socket
import string
import sys
import re #regesp

CON_LITERAL = b'\a\b'
SERVER_KEY_REQUEST = '107 KEY REQUEST\a\b'.encode()
SERVER_OK = '200 OK\a\b'.encode()
SERVER_LOGIN_FAILED = '300 LOGIN FAILED\a\b'.encode()

SERVER_MOVE = '102 MOVE\a\b'.encode()
SERVER_TURN_LEFT = '103 TURN LEFT\a\b'.encode()
SERVER_TURN_RIGHT = '104 TURN RIGHT\a\b'.encode()
SERVER_PICK_UP = '105 GET MESSAGE\a\b'.encode()
SERVER_LOGOUT = '106 LOGOUT\a\b'.encode()

CLIENT_RECHARGING = 'RECHARGING\a\b'.encode()
CLIENT_FULL_POWER = 'FULL POWER\a\b'.encode()

NORTH = 1
SOUTH = 2
EAST = 3
WEST = 4

###Errors
SERVER_KEY_OUT_OF_RANGE_ERROR = '303 KEY OUT OF RANGE\a\b'.encode()
SERVER_SYNTAX_ERROR = '301 SYNTAX ERROR\a\b'.encode()
SERVER_LOGIC_ERROR = '302 LOGIC ERROR\a\b'.encode()


def Coordinate(data):
    #control syntax CLIENT_OK like "OK 3 7\a\b"
    x = 0
    y = 0
    data1 = data.decode()
    tmp_str = data1[3:][:-2].split(' ')
    if len(tmp_str) > 2:
        return 999,999
    #floating point
    try:
        x = int(tmp_str[0])
        y = int(tmp_str[1])
    except:
        return 999,999
    return x,y


def turn_around(smer,data_buff):
    conn.send(SERVER_TURN_RIGHT)
    data,data_buff = catch_data(4096,data_buff,12)
    conn.send(SERVER_TURN_RIGHT)
    data,data_buff = catch_data(4096,data_buff,12)

    if(smer == NORTH):
        return SOUTH
    elif(smer == EAST):
        return WEST
    elif(smer == SOUTH):
        return NORTH
    else:
        return EAST

def turn_rught(smer,data_buff):
    conn.send(SERVER_TURN_RIGHT)
    data,data_buff = catch_data(4096,data_buff,12)
    
    if(smer == NORTH):
        return EAST
    elif(smer == EAST):
        return SOUTH
    elif(smer == SOUTH):
        return WEST
    else:
        return NORTH

def turn_left(smer,data_buff):
    conn.send(SERVER_TURN_LEFT)
    data,data_buff = catch_data(4096,data_buff,12)
    
    if(smer == NORTH):
        return WEST
    elif(smer == WEST):
        return SOUTH
    elif(smer == SOUTH):
        return EAST
    else:
        return NORTH



def move_x(data_buff, smer, x_curr,y_curr):
    if(x_curr <= 0):
        if(smer == EAST):
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
        elif(smer == WEST):
            smer = turn_around(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == SOUTH):
            smer = turn_left(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == NORTH):
            smer = turn_rught(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
    else:#(x_curr > 0):
        if(smer == WEST):
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == EAST):
            smer = turn_around(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == NORTH):
            smer = turn_left(smer,data_buff)          
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)

        elif(smer == SOUTH):
            smer = turn_rught(smer,data_buff)         
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
    return x_curr,y_curr,smer


def move_y(data_buff, smer, x_curr, y_curr):

    if(y_curr <= 0):
        if(smer == NORTH):
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == SOUTH):
            smer = turn_around(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == EAST):
            smer = turn_left(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == WEST):
            smer = turn_rught(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
    else:#(x_curr > 0):
        if(smer == SOUTH):
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == NORTH):
            smer = turn_around(smer,data_buff)
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
 
        elif(smer == WEST):
            smer = turn_left(smer,data_buff)          
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)

        elif(smer == EAST):
            smer = turn_rught(smer,data_buff)         
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x_curr,y_curr = Coordinate(data)
    return x_curr,y_curr,smer





def Move_R(data_buff):
    conn.send(SERVER_TURN_RIGHT)
    data,data_buff = catch_data(4096,data_buff,12)
    conn.send(SERVER_MOVE)
    data,data_buff = catch_data(4096,data_buff,12)

def catch_data(size,data_buff,max_len):
    data_recv = b''
    data_back = b''
    
    while True:
        if(len(data_buff) >= max_len and bytearray(data_buff).find(CON_LITERAL) == -1):
            data_back = data_buff[0:max_len]
            return data_back,data_buff

        #logical errror


        # buffer is empty
        if data_buff == b'':
            try:
                data_recv = conn.recv(size)
                if data_recv == CLIENT_RECHARGING:
                    conn.settimeout(5)
                    data_recv = b''
                    data_recv = conn.recv(size)
                    if data_recv == CLIENT_FULL_POWER:
                        conn.settimeout(1)
                        data_recv = b''
                        continue
                    else:
                        conn.send(SERVER_LOGIC_ERROR)
                        conn.close()
                        break

                data_buff += data_recv
                if data_buff == CLIENT_RECHARGING:
                    conn.settimeout(5)
                    data_recv = conn.recv(size)
                    conn.settimeout(1)
                    if data_recv == CLIENT_FULL_POWER:
                        continue
                    else:
                        conn.send(SERVER_LOGIC_ERROR)
                        conn.close()
                        break
                        

            except:
                break

        elif(data_buff != b''):
            var1 = bytearray(data_buff).find(CON_LITERAL)

            ch_flag = bytearray(data_buff).find(CLIENT_RECHARGING)
            data_buff=data_buff.replace(CLIENT_RECHARGING,b'')

            if(var1 == -1):
                data_recv = conn.recv(size)
                if data_buff == CLIENT_RECHARGING:
                    conn.settimeout(5)
                    data_recv = conn.recv(size)
                    conn.settimeout(1)
                    if data_recv == CLIENT_FULL_POWER:
                        data_recv = b''
                        continue
                    else:
                        conn.send(SERVER_LOGIC_ERROR)
                        conn.close()
                        break
                data_buff += data_recv
                continue
            elif(ch_flag != -1):
                conn.settimeout(5)              
                data_recv = conn.recv(size)
                if data_recv == CLIENT_FULL_POWER:
                    conn.settimeout(1)
                    continue
            else:
                data_back,data_buff = data_cut(data_buff)
                return data_back,data_buff
        else:
            break
    
     

def data_cut(data_buff):
    len = bytearray(data_buff).find(CON_LITERAL)

    data_back = data_buff[0:len+2]
    data_buff = data_buff[len+2:]
    return data_back,data_buff


### kontrola poctu argumentu
if(len(sys.argv)) == 1:
    print ('Usage: server port unrecognize')
    sys.exit(-1)


### Inicializace socketu
serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)

### kontrola portu
port = int(sys.argv[1])
if(port == 0):
    print ('Usage: server port is zerrobytearray')
    serv_sock.close()
    sys.exit(-1)


### Prirazeni socketu k rozhranim
serv_sock.bind(('', port)) 


### Oznacim socket jako pasivni
serv_sock.listen(5)          



while True:
    data_buff = b''
    data = b''

    print("Wait new accept() from client-side")
    ### Cekam na prichozi spojeni
    conn, addr = serv_sock.accept()

    try:
        conn.settimeout(1)
        while True:
            data,data_buff = catch_data(4096,data_buff,20)

            if (len(data[:-2]) > 18 or data[-2:] != CON_LITERAL):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break

            #=====================================
            #Autentizace
            #=====================================
            client_username = (data[:-2])

            # Send SERVER_KEY_REQUEST
            conn.send(SERVER_KEY_REQUEST)

            # Rec CLIENT_KEY_ID max_len == 5
            try:
                data,data_buff = catch_data(4096,data_buff,7)
            except Exception:
                conn.close()
                break
            if (len(data[:-2]) > 3 or data[-2:] != CON_LITERAL):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break
            
            # Check syntax CLIENT_KEY_ID
            try:
                id = int(data[:-2])
            except:
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break

            # Vysledny hash
            # (sum(ASCII_client_username) *1000) % 65536
            hash = sum(bytearray(client_username))
            hash = (hash * 1000) % 65536

            # Pricitam Server Key -- potvrzovací kód serveru
            # (hash) + Server_Key % 65536
            if(id == 0):
                server_confirm = (hash + 23019) % 65536
                client_information_serv_side = (hash + 32037) % 65536
            elif(id == 1):
                server_confirm = (hash + 32037) % 65536
                client_information_serv_side = (hash + 29295) % 65536
            elif(id == 2):
                server_confirm = (hash + 18789) % 65536
                client_information_serv_side = (hash + 13603) % 65536
            elif(id == 3):
                server_confirm = (hash + 16443) % 65536
                client_information_serv_side = (hash + 29533) % 65536
            elif(id == 4):
                server_confirm = (hash + 18189) % 65536
                client_information_serv_side = (hash + 21952) % 65536
            elif(type(id) == int):
                conn.send(SERVER_KEY_OUT_OF_RANGE_ERROR)
                conn.close()
                break

            # Send SERVER_CONFIRM
            val = str(server_confirm).encode() + CON_LITERAL
            conn.send(val)

            # Rec CLIENT_CONFIRMATION max_len = 7
            try:
                data,data_buff = catch_data(4096,data_buff,7)
            except Exception:
                conn.close()
                break

            if (len(data[:-2]) > 5 or data[-2:] != CON_LITERAL):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break




            if(' ' in str(data[:-2])):
                conn.send(SERVER_SYNTAX_ERROR)
                print("send SERVER_SYNTAX_ERROR white space in string")
                conn.close()  
                break
            
            try:
                client_information = int(data[:-2])

            except ValueError:
                print("send SERVER_SYNTAX_ERROR client_information")
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break

            if(len(str(client_information)) > 5):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break

            # Porovnani Hash client
            if(id == 0 and client_information == client_information_serv_side):
                conn.send(SERVER_OK)       
            elif(id == 1 and client_information == client_information_serv_side):
                conn.send(SERVER_OK)       
            elif(id == 2 and client_information == client_information_serv_side):
                conn.send(SERVER_OK)       
            elif(id == 3 and client_information == client_information_serv_side):
                conn.send(SERVER_OK)       
            elif(id == 4 and client_information == client_information_serv_side):
                conn.send(SERVER_OK)       
            else:
                conn.send(SERVER_LOGIN_FAILED)
                conn.close()
                break

                    
            #=====================================
            #Pohyb
            #=====================================
            # Zacatek
            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x1,y1 = Coordinate(data)
            if(x1 == 999):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()           
                break


            conn.send(SERVER_MOVE)
            data,data_buff = catch_data(4096,data_buff,12)
            x2,y2 = Coordinate(data)
            if(x2 == 999):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()           
                break
            elif(x1 == x2 and y1 == y2):
                (x1,y1) = (x2,y2)
                conn.send(SERVER_TURN_RIGHT)
                data,data_buff = catch_data(4096,data_buff,12)
                conn.send(SERVER_MOVE)
                data,data_buff = catch_data(4096,data_buff,12)
                x2,y2 = Coordinate(data)


            
            if (x1 > x2):
                smer = WEST
            elif(x1 < x2):
                smer = EAST
            elif(y1 > y2):
                smer = SOUTH
            else:
                smer = NORTH



            # pohyb k cili

            x_prev = x1
            y_prev = y1
            x_curr = x2
            y_curr = y2
            cnt = 0
            while(x_curr != 0 or y_curr != 0):
                if( cnt > 20 ):
                    conn.close()
                    break
                if(x_curr != 0):
                    x_prev,y_prev = x_curr,y_curr #store last position
                    x_curr,y_curr,smer = move_x(data_buff, smer, x_curr,y_curr)

                    if(x_curr == x_prev and y_curr == y_prev):
                        cnt = cnt + 1
                        x_prev,y_prev = x_curr,y_curr #store last position
                        x_curr,y_curr,smer = move_y(data_buff, smer, x_curr,y_curr)

                elif(y_curr != 0):      
                    x_prev,y_prev = x_curr,y_curr #store last position
                    x_curr,y_curr,smer = move_y(data_buff, smer, x_curr,y_curr)

                    if(x_curr == x_prev and y_curr == y_prev):
                        cnt = cnt + 1
                        x_prev,y_prev = x_curr,y_curr #store last position
                        x_curr,y_curr,smer = move_x(data_buff, smer, x_curr,y_curr)
                        x_curr,y_curr,smer = move_y(data_buff, smer, x_curr,y_curr)
                        x_curr,y_curr,smer = move_y(data_buff, smer, x_curr,y_curr)
                        x_curr,y_curr,smer = move_x(data_buff, smer, x_curr,y_curr)
                else:
                    break





            conn.send(SERVER_PICK_UP)
            try:
                data,data_buff = catch_data(4096,data_buff,100)
            except:
                print('1')
                conn.close()
                break

            print("SECRET MSG == ",data[:-2])

            if (len(data[:-2]) > 98 or data[-2:] != CON_LITERAL):
                conn.send(SERVER_SYNTAX_ERROR)
                conn.close()
                break

            #last_check
            
            conn.send(SERVER_LOGOUT)
            conn.close()
            break


    except socket.timeout:
        print('1')
    finally:
        # Clean up the connection
        print('Caught timeout')
        conn.close()
