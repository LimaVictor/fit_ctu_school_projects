create or replace procedure SMAZ_VSECHNY_TABULKY AS
begin
  for iRec in 
    (select distinct OBJECT_TYPE, OBJECT_NAME,
      'drop '||OBJECT_TYPE||' "'||OBJECT_NAME||'"'||
      case OBJECT_TYPE when 'TABLE' then ' cascade constraints purge' else ' ' end as PRIKAZ
    from USER_OBJECTS where OBJECT_NAME not in ('SMAZ_VSECHNY_TABULKY', 'VYPNI_CIZI_KLICE', 'ZAPNI_CIZI_KLICE', 'VYMAZ_DATA_VSECH_TABULEK')
    ) loop
        begin
          dbms_output.put_line('Prikaz: '||irec.prikaz);
        execute immediate iRec.prikaz;
        exception
          when others then dbms_output.put_line('NEPOVEDLO SE!');
        end;
      end loop;
end;
/

create or replace procedure VYPNI_CIZI_KLICE as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" DISABLE';
  end loop;
end VYPNI_CIZI_KLICE;
/


create or replace procedure ZAPNI_CIZI_KLICE as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" enable validate';
  end loop;
end ZAPNI_CIZI_KLICE;
/

create or replace procedure VYMAZ_DATA_VSECH_TABULEK is
begin
  -- Vymazat data vsech tabulek
  VYPNI_CIZI_KLICE;
  for v_rec in (select distinct TABLE_NAME from USER_TABLES)
  loop
    execute immediate 'truncate table '||v_rec.TABLE_NAME||' drop storage';
  end loop;
  ZAPNI_CIZI_KLICE;
  
  -- Nastavit vsechny sekvence od 1
  for v_rec in (select distinct SEQUENCE_NAME  from USER_SEQUENCES)
  loop
    execute immediate 'alter sequence '||v_rec.SEQUENCE_NAME||' restart start with 1';
  end loop;
end VYMAZ_DATA_VSECH_TABULEK;
/


--Zrusit stare tabulky =======================================================


exec SMAZ_VSECHNY_TABULKY;


--script =======================================================

CREATE TABLE blacklist (
    id_zak     INTEGER NOT NULL,
    poznamka   VARCHAR2(520 CHAR)
);

ALTER TABLE blacklist ADD CONSTRAINT pk_blacklist PRIMARY KEY ( id_zak );

CREATE TABLE pokoj (
    cislo_pokoje   INTEGER NOT NULL,
    patro          INTEGER NOT NULL,
    pocet_luzek    INTEGER NOT NULL,
    cena           INTEGER NOT NULL
);

ALTER TABLE pokoj ADD CONSTRAINT pk_pokoj PRIMARY KEY ( cislo_pokoje );

CREATE TABLE pozice (
    id_zam   INTEGER NOT NULL,
    pozice   VARCHAR2(100) NOT NULL
);

ALTER TABLE pozice ADD CONSTRAINT pk_pozicev1 PRIMARY KEY ( id_zam );

CREATE TABLE pracovnik_externista (
    id_pra            INTEGER NOT NULL,
    jmeno_ext         VARCHAR2(100) NOT NULL,
    prijmeni_ext      VARCHAR2(100) NOT NULL,
    pozice_ext        VARCHAR2(100) NOT NULL,
    telefonni_cislo   VARCHAR2(100) NOT NULL,
    firma             VARCHAR2(100)
);

ALTER TABLE pracovnik_externista ADD CONSTRAINT pk_pracovnik_externista PRIMARY KEY ( id_pra );

CREATE TABLE pujcovna (
    inv_cislo   VARCHAR2(100) NOT NULL,
    vec         VARCHAR2(100) NOT NULL,
    pocet_dnu   INTEGER NOT NULL
);

ALTER TABLE pujcovna ADD CONSTRAINT pk_pujcovna PRIMARY KEY ( inv_cislo );

CREATE TABLE rezervace_sluzby (
    id_rezervace_sl   INTEGER NOT NULL,
    id_zam            INTEGER NOT NULL,
    id_zak            INTEGER NOT NULL,
    datum             DATE NOT NULL,
    inv_cislo         VARCHAR2(100),
    cas               DATE,
    cena              INTEGER NOT NULL,
    kod_sluzba        VARCHAR2(50)
);

ALTER TABLE rezervace_sluzby
    ADD CONSTRAINT arc_1 CHECK ( ( ( inv_cislo IS NOT NULL )
                                   AND ( kod_sluzba IS NULL ) )
                                 OR ( ( kod_sluzba IS NOT NULL )
                                      AND ( inv_cislo IS NULL ) ) );

ALTER TABLE rezervace_sluzby ADD CONSTRAINT pk_rezervace_sluzby PRIMARY KEY ( id_rezervace_sl );

CREATE TABLE rezervace_ubytovani (
    id_rez           INTEGER NOT NULL,
    id_zam           INTEGER NOT NULL,
    datum_rezervace  DATE NOT NULL,
    id_zak           INTEGER NOT NULL,
    prijezd          DATE NOT NULL,
    odjezd           DATE NOT NULL,
    pocet_hostu      INTEGER,
    cislo_pokoje     INTEGER NOT NULL
);

ALTER TABLE rezervace_ubytovani ADD CONSTRAINT pk_rezervace_ubytovani PRIMARY KEY ( id_rez );

CREATE TABLE rozvrh_externista (
    id_pra       INTEGER NOT NULL,
    datum        DATE NOT NULL,
    akce         VARCHAR2(100) NOT NULL,
    pocet_lidi   INTEGER
);

ALTER TABLE rozvrh_externista ADD CONSTRAINT pk_rozvrh_externista PRIMARY KEY ( datum,
                                                                                akce );

CREATE TABLE sluzba (
    kod_sluzba     VARCHAR2(50) NOT NULL,
    nazev_sluzby   VARCHAR2(100) NOT NULL,
    poznamka       VARCHAR2(100)
);

ALTER TABLE sluzba ADD CONSTRAINT pk_sluzba PRIMARY KEY ( kod_sluzba );

CREATE TABLE sluzba_externi (
    id_sluzbaex   INTEGER NOT NULL,
    id_zak        INTEGER NOT NULL,
    datum         DATE NOT NULL,
    akce          VARCHAR2(100) NOT NULL
);

ALTER TABLE sluzba_externi ADD CONSTRAINT pk_sluzba_externi PRIMARY KEY ( id_sluzbaex );

CREATE TABLE stravovani (
    id_rez     INTEGER NOT NULL,
    typ_012    INTEGER NOT NULL,
    vegan_an   CHAR(20),
    poznamka   VARCHAR2(255),
    cena       INTEGER NOT NULL
);

ALTER TABLE stravovani ADD CHECK ( typ_012 BETWEEN 0 AND 2 );

ALTER TABLE stravovani ADD CONSTRAINT pk_stravovani PRIMARY KEY ( typ_012,
                                                                  id_rez );

CREATE TABLE zakaznik (
    id_zak            INTEGER NOT NULL,
    jmeno             VARCHAR2(100) NOT NULL,
    prijmeni          VARCHAR2(100) NOT NULL,
    datum_narozeni    DATE NOT NULL,
    telefonni_cislo   VARCHAR2(100) NOT NULL,
    email             VARCHAR2(100),
    kod_stat          VARCHAR2(3 CHAR)
);

ALTER TABLE zakaznik ADD CONSTRAINT pk_zakaznik PRIMARY KEY ( id_zak );

CREATE TABLE zamestnanec (
    id_zam              INTEGER NOT NULL,
    jmeno_zam           VARCHAR2(100) NOT NULL,
    prijmeni_zam        VARCHAR2(100) NOT NULL,
    datum_nastupu       DATE NOT NULL,
    datum_ukonceni_pp   DATE,
    telefonni_cislo     VARCHAR2(100),
    mzda                NUMBER NOT NULL
);

ALTER TABLE zamestnanec ADD CONSTRAINT pk_zamestnanec PRIMARY KEY ( id_zam );

ALTER TABLE blacklist
    ADD CONSTRAINT fk_blacklist_zakaznik FOREIGN KEY ( id_zak )
        REFERENCES zakaznik ( id_zak );

ALTER TABLE pozice
    ADD CONSTRAINT fk_pozice_zamestnanec FOREIGN KEY ( id_zam )
        REFERENCES zamestnanec ( id_zam );

ALTER TABLE rezervace_sluzby
    ADD CONSTRAINT fk_rez_sluzby_pujcovna FOREIGN KEY ( inv_cislo )
        REFERENCES pujcovna ( inv_cislo );

ALTER TABLE rezervace_sluzby
    ADD CONSTRAINT fk_rez_sluzby_sluzba FOREIGN KEY ( kod_sluzba )
        REFERENCES sluzba ( kod_sluzba );

ALTER TABLE rezervace_sluzby
    ADD CONSTRAINT fk_rez_sluzby_zak FOREIGN KEY ( id_zak )
        REFERENCES zakaznik ( id_zak );

ALTER TABLE rezervace_ubytovani
    ADD CONSTRAINT fk_rez_ubytovani_pokoj FOREIGN KEY ( cislo_pokoje )
        REFERENCES pokoj ( cislo_pokoje );

ALTER TABLE rezervace_ubytovani
    ADD CONSTRAINT fk_rez_ubytovani_zak FOREIGN KEY ( id_zak )
        REFERENCES zakaznik ( id_zak );

ALTER TABLE rezervace_ubytovani
    ADD CONSTRAINT fk_rez_ubytovani_zam FOREIGN KEY ( id_zam )
        REFERENCES zamestnanec ( id_zam );

ALTER TABLE rezervace_sluzby
    ADD CONSTRAINT fk_reze_sluzby_zam FOREIGN KEY ( id_zam )
        REFERENCES zamestnanec ( id_zam );

ALTER TABLE rozvrh_externista
    ADD CONSTRAINT fk_rozvrh_ext_pracovnik_ext FOREIGN KEY ( id_pra )
        REFERENCES pracovnik_externista ( id_pra );

ALTER TABLE sluzba_externi
    ADD CONSTRAINT fk_sluzba_externi_zakaznik FOREIGN KEY ( id_zak )
        REFERENCES zakaznik ( id_zak );

ALTER TABLE sluzba_externi
    ADD CONSTRAINT fk_sluzba_exti_rozvrh_ext FOREIGN KEY ( datum,
                                                           akce )
        REFERENCES rozvrh_externista ( datum,
                                       akce );

ALTER TABLE stravovani
    ADD CONSTRAINT fk_stravovani_rez_ubytovani FOREIGN KEY ( id_rez )
        REFERENCES rezervace_ubytovani ( id_rez );

