# BI-DBS Semestral work

## Popis

Společnost "Small Paradise" vlastní stejnojmenný hotel v malé horské vesnici. Hotel nabízi celoroční ubytování v jednolůžkových, dvoulůžkových a třílůžkových pokojích. Ubytování je vhodné jak pro rodiny s dětmi, party kamarádů tak i pro lidi, kteří cestují sami.

Při tvorbě rezervací si zákazník vybere pokoj, datum příjezdu, odjezdu a počet hostů.

Hosté můžou mít předem zaplacené stravování a to buď snídaně, snídaně a večeře, nebo nic. Čerstvé jídlo do hotelu dovážejí každý den, takže pracovníci restaurace by měli vědět předem, pro kolik lidí budou potřebovat objednat jídlo.

Správa hotelu má dohodu se zdejším obyvatelem (průvodce), který přes léto organizuje jednodenní nebo třídenní pěší výlety na hory. Kvůli nízkemu zájmu se výlety konají jen párkrát za týden. Průvodce sestavuje vlastní rozvrh a host si může vybrat den, ve který by chtěl jít na výlet. V zimě lyžařští instruktoři (externí zaměstnanec) rádi naučí nováčky lyžovat nebo jezdit na snowboardu. Hotel sbírá informace o počtu zejemců o externí služby, aby je v budoucnu mohli zlepšit.

V hotelu je celoročně otevřena vlastní půjčovna, kde si zákazník může půjčit gril, různé sportovní, turistické vybavení a v zimě navíc vybavení lyžařské. Do databáze by se mělo ukládat ID zákazníka, který si vybavení půjčí a na jak dlouhou dobu.

Pro kompletní odpočinek se dají využít další služby. Zarezervovat saunu nebo si zahrát kulečník s kamarády. Při využití těchto služeb musí pracovník uvést kdy klíče půjčil, na jak dlouho, ID zákazníka a informaci o vrácení klíčů.

```
zakaznik (ID_zak, Jmeno, prijmeni, datum_narozeni, email, telefonni_cislo)
rezervace (ID_rez, datum_rezrvace, prijezd, odjezd, stravování, pocet_hostu)
zamnestnanec (ID_zam, jmeno_z, prijmeni_z, pozice)
pracovnik_externista (ID_pre, jmeno_e, prijmeni_e, pozice)
pokoj (ID_pok, datum)
sluzvy (ID_slu, datum, cas, poznamka)
rozvrh_externi (datum, aktivita)
sluzby_esxterni (ID_sle, den, pocet_lidi)
stravovani (ID_jid, typ)
pujcovna (ID_puj, Vec, pocet_dnu)
```

# Relational Schema
![relational_schema](relational_schema.png)
