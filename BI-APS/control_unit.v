//!---------------------
//!----control_unit-----
//!---------------------
module control_unit ( input [5:0] Opcode ,
                      input [5:0] funct,
                      input [4:0] shamt,
                      output [3:0] ALUControl,
                      output RegWrite ,
                      output RegDst ,
                      output ALUSrc ,
                      output Branch ,
                      output MemWrite ,
                      output MemToReg ,
                      output PCSrcJal ,
                      output PCSrcJr );

	wire [1:0] cu_wire;
	main_decoder md(Opcode, cu_wire, RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr);
	ALU_op_decoder op_dec(funct, shamt, cu_wire, ALUControl);
endmodule



//!--------------------
//!------main_decoder--
//!--------------------
module main_decoder ( input [5:0] Opcode ,
                      output reg [1:0] ALUOp ,
                      output reg RegWrite ,
                      output reg RegDst ,
                      output reg ALUSrc ,
                      output reg Branch ,
                      output reg MemWrite ,
                      output reg MemToReg ,
                      output reg PCSrcJal ,
                      output reg PCSrcJr );

	always@(*)
	  case (Opcode)
 	  0:	begin		//R typ
		RegWrite = 1;
		RegDst = 1;
		ALUSrc = 0;		
		ALUOp = 2;
		Branch = 0;
		MemWrite = 0;
		MemToReg = 0;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	  35: 	begin		//lw
		RegWrite = 1;
		RegDst = 0;
		ALUSrc = 1;		
		ALUOp = 0;
		Branch = 0;
		MemWrite = 0;
		MemToReg = 1;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	  43: 	begin		//sw
		RegWrite = 0;
		ALUSrc = 1;		
		ALUOp = 0;
		Branch = 0;
		MemWrite = 1;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	   4: 	begin		//beq
		RegWrite = 0;
		ALUSrc = 0;		
		ALUOp = 1;
		Branch = 1;
		MemWrite = 0;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	   8:	begin		//addi
		RegWrite = 1;
		RegDst = 0;
		ALUSrc = 1;		
		ALUOp = 0;
		Branch = 0;
		MemWrite = 0;
		MemToReg = 0;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	   3:	begin		//jal
		RegWrite = 1;		
		MemWrite = 0;
		PCSrcJal = 1;
		PCSrcJr = 0;
		end
	   2:	begin		//!j Op code is 2
		RegWrite = 0;	//RegWrite in j is false, in jal is true
		MemWrite = 0;
		PCSrcJal = 1;
		PCSrcJr = 0;
		end
	   7:	begin		//jr
		RegWrite = 0;		
		MemWrite = 0;
		PCSrcJal = 0;
		PCSrcJr = 1;
		end
	   31:	begin		//addu[_s].qb
		RegWrite = 1;
		RegDst = 1;
		ALUSrc = 0;		
		ALUOp = 3;
		Branch = 0;
		MemWrite = 0;
		MemToReg = 0;
		PCSrcJal = 0;
		PCSrcJr = 0;
		end
	  endcase
endmodule

//!--------------------
//!------ALU_op_decoder
//!--------------------
module ALU_op_decoder ( input [5:0] funct, 
                        input [4:0] shamt,   
                        input [1:0] ALUOp, 
                        output reg [3:0] ALUControl);

	always@(*)
		if (ALUOp==0)               //additional (lw,sw,addi)
			ALUControl=2; 

		else if (ALUOp==1)          //subtraction (beq)
			ALUControl=6;

		else if (ALUOp==2)          // R-type
			case (funct)
				4  : ALUControl=4;  // sllv 
				6  : ALUControl=10; // srlv
				7  : ALUControl=11; // srav
				32 : ALUControl=2;  // add
				34 : ALUControl=6;  // sub
				36 : ALUControl=0;  // and
				37 : ALUControl=1;  // or
				42 : ALUControl=7;  // slt
			endcase

		else if (ALUOp==3 && funct==16)
			case (shamt)
				0 : ALUControl=8;   // addu.qb
				4 : ALUControl=9;   // addu_s.qb	
			endcase
endmodule