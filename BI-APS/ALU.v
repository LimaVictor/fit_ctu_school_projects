//!----------------
//!----ALU_32bit---
module ALU_32bit ( input [31:0] SrcA , SrcB ,
                   input [3:0] ALUControl,
                   output reg [31:0] ALUResult ,
                   output reg zero );

	reg [8:0]a;

	always@(*) begin
	  zero=0;
	case (ALUControl)
	4:  ALUResult = SrcB << SrcA;   // sllv
	10: ALUResult = SrcB >> SrcA;   // srlv unsigned
	11: ALUResult = SrcB >>> SrcA;	// srav signed
	2:  ALUResult = SrcA + SrcB;    // add
	6:  ALUResult = SrcA - SrcB;    // sub
	0:  ALUResult = SrcA & SrcB;    // and
	1:  ALUResult = SrcA | SrcB;    // or
	3:  ALUResult = SrcA ^ SrcB;    // xor
	
	// slt (set less than)
	7: 	begin
			if (SrcA[31] == 1 && SrcB[31] == 1) ALUResult = SrcA > SrcB;
			else if (SrcA[31] == 1) ALUResult = 1;
			else if (SrcB[31] == 1) ALUResult = 0;
			else ALUResult = SrcA < SrcB;
		end
		
	// addu.qb	
	8:	begin
			ALUResult[7:0] = SrcA[7:0] + SrcB[7:0];
			ALUResult[15:8] = SrcA[15:8] + SrcB[15:8];
			ALUResult[23:16] = SrcA[23:16] + SrcB[23:16];
			ALUResult[31:24] = SrcA[31:24] + SrcB[31:24];
		end
		
	// addu_s.qb		
	9:	begin
			a[8:0] = SrcA[7:0] + SrcB[7:0];
			if (a[8] == 1) ALUResult[7:0] = 8'b11111111;
			else ALUResult[7:0] = a[7:0];

			a[8:0] = SrcA[15:8] + SrcB[15:8];
			if (a[8] == 1) ALUResult[15:8] = 8'b11111111;
			else ALUResult[15:8] = a[7:0];

			a[8:0] = SrcA[23:16] + SrcB[23:16];
			if (a[8] == 1) ALUResult[23:16] = 8'b11111111;
			else ALUResult[23:16] = a[7:0];

			a[8:0] = SrcA[31:24] + SrcB[31:24];
			if (a[8] == 1) ALUResult[31:24] = 8'b11111111;
			else ALUResult[31:24] = a[7:0];
		end
		
	endcase
		if (ALUResult==0)
		zero=1;
	end
	
endmodule