`include "reg_file.v"
`include "sign_ext.v"
`include "control_unit.v"
`include "ALU.v"
`include "reg_32bit_res.v"


module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem );
	wire [31:0] sign_ext_mux, sh_add, w_pcbranch, w_pcjal, w_mux4_reg, w_pc;
	wire [31:0] srcb_alu, srca_alu, w_resault, w_mux4, w_wd_reg;
	wire [4:0] w_writereg, w_a3;
	wire [3:0] w_alucontr;
	wire w_regwrite, w_regdst, w_alusrc, w_branch, w_memtoreg, w_pcsrcjal, w_pcsrcjr, w_zero, w_pcsrcbeq;
//BUS
	assign w_pcjal[31:28] = w_mux4[31:28];
	assign w_pcjal[27:2] = instruction[25:0];
	assign w_pcjal[1:0] = 2'b00;
//mod descr
	sext sign_ext(instruction[15:0], sign_ext_mux);
	mux_2_1 mux_1(sign_ext_mux, data_to_mem, w_alusrc, srcb_alu);
	mux_2_1_5b mux_2_5(instruction[15:11], instruction[20:16], w_regdst, w_writereg[4:0]);
	mux_2_1_5b mux_5_5(5'b11111, w_writereg, w_pcsrcjal, w_a3[4:0]); 
	mux_2_1 mux_3(data_from_mem, address_to_mem, w_memtoreg, w_resault);
	mux_2_1 mux_4(w_mux4, w_resault, w_pcsrcjal, w_wd_reg);
	mux_4_1 mux_4bit(srca_alu, w_pcjal, w_pcbranch, w_mux4, w_pcsrcjr, w_pcsrcjal, w_pcsrcbeq, w_mux4_reg);
	reg_32bit_res reg32(w_mux4_reg, clk, reset, PC);
	adder_32bit add1(sh_add, w_mux4, w_pcbranch);
	adder_32bit add2(4, PC, w_mux4); //*const
	and_m and_mod(w_branch, w_zero, w_pcsrcbeq);
	shift_32bit shiftl(sign_ext_mux, sh_add);
	ALU_32bit alu1(srca_alu, srcb_alu, w_alucontr, address_to_mem, w_zero);
	control_unit cntrunit(instruction[31:26], instruction[5:0], instruction[10:6], w_alucontr, w_regwrite, w_regdst, w_alusrc, w_branch, WE, w_memtoreg, w_pcsrcjal, w_pcsrcjr); //we - mem_write
	reg_file regfile(instruction[25:21], instruction[20:16], w_a3, w_wd_reg, clk, w_regwrite, reset, srca_alu, data_to_mem); 
endmodule


//!----------------
//!----mux_2_1-----
module mux_2_1 (input [31:0] d0, d1,
                input select,
                output reg [31:0] y);
  always@(*)
	if(select) y = d0; 
	else  y = d1;
endmodule


//!----------------
//!----mux_2_1_5b-----
module mux_2_1_5b (input [4:0] d0, d1,
                   input select,
                   output reg [4:0] y);
  always@(*)
	if(select) y = d0; 
	else  y = d1;
endmodule


//!----------------
//!----mux_4_1-----
module mux_4_1 (input [31:0] d0, d1, d2, d3,
                input  select0, select1, select2,
                output reg [31:0] y);
  always@(*)
	if (select0==1) y = d0;
	else if (select1==1) y = d1;
	else if (select2==1) y = d2;
	else y = d3;
endmodule


//!------------------
//!----shift_32bit---
module shift_32bit ( input [31:0] a ,
                     output [31:0] b);
	assign b = a << 2;
endmodule

//!------------------
//!------AND---------
module and_m ( input a, b,
               output reg c);
	always@(*)
		if (a==1 && b==1) c=1;
		else c=0;
endmodule


//!----------------------
//!---adder_32bit---------
module adder_32bit ( input [31:0] a , b ,
                     output [31:0] s );
	assign s = a + b ;
endmodule