//sign extension 16bit to 32bit

module sext ( input [15:0] a,
              output reg [31:0] s);
	always @(*)
		if (a[15]==0)
		s=a;
		else
		s={ {16{a[15]}}, a } ;
endmodule