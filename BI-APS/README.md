My implementation of 32-bit Single cycle processor in verilog

Bubble Sort in Assembly 
Calling convention O32

## CPU Modules

```verilog
module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem );
```
```verilog
module control_unit ( input [5:0] Opcode ,
                      input [5:0] funct,
                      input [4:0] shamt,
                      output [3:0] ALUControl,
                      output RegWrite ,
                      output RegDst ,
                      output ALUSrc ,
                      output Branch ,
                      output MemWrite ,
                      output MemToReg ,
                      output PCSrcJal ,
                      output PCSrcJr );
```

```verilog
module main_decoder ( input [5:0] Opcode ,
                      output reg [1:0] ALUOp ,
                      output reg RegWrite ,
                      output reg RegDst ,
                      output reg ALUSrc ,
                      output reg Branch ,
                      output reg MemWrite ,
                      output reg MemToReg ,
                      output reg PCSrcJal ,
                      output reg PCSrcJr );
```

```verilog
module ALU_op_decoder ( input [5:0] funct, 
                        input [4:0] shamt,   
                        input [1:0] ALUOp, 
                        output reg [3:0] ALUControl);
```

```verilog
module ALU_32bit ( input [31:0] SrcA , SrcB ,
                   input [3:0] ALUControl,
                   output reg [31:0] ALUResult ,
                   output reg zero );
```
```verilog
module main_decoder ( input [5:0] Opcode ,
                      output reg [1:0] ALUOp ,
                      output reg RegWrite ,
                      output reg RegDst ,
                      output reg ALUSrc ,
                      output reg Branch ,
                      output reg MemWrite ,
                      output reg MemToReg ,
                      output reg PCSrcJal ,
                      output reg PCSrcJr );
```

```verilog
module reg_32bit_res ( input [31:0] datain,
                       input clk, reset, 
                       output reg [31:0] dataout);
```

```verilog
module reg_file ( input [4:0] a1 , a2 , a3,
                  input [31:0] wd3, //save
                  input clk, we3, reset,
                  output reg [31:0] rd1 , rd2);
```

```verilog
module sext ( input [15:0] a,
              output reg [31:0] s);
```

## CPU instruction set #

| Instruction | Syntax | Operation | Encoding |
| ------ | ------ | ------ | ------ |
| add | add d, s, t | d = s + t | `0000 00ss ssst tttt dddd d000 0010 0000` |
| sub | sub d, s, t | d = s - t | `0000 00ss ssst tttt dddd d000 0010 0010` |
| and | and d, s, t | d = s & t | `0000 00ss ssst tttt dddd d000 0010 0100` |
| or | or d, s, t | d = s \| t | `0000 00ss ssst tttt dddd d000 0010 0101` |
| slt | slt d, s, t | d = (s<t) ? 1 : 0 | `0000 00ss ssst tttt dddd d000 0010 1010` |
| addi | addi t, s, imm | t = s + imm | `0010 00ss ssst tttt iiii iiii iiii iiii` |
| lw | lw t, offset(s) | t = MEM[s + offset] | `1000 11ss ssst tttt iiii iiii iiii iiii` |
| sw | sw t, offset(s) | MEM[s + offset] = t | `1010 11ss ssst tttt iiii iiii iiii iiii` |
| beq | beq s, t, offset | | `0001 00ss ssst tttt iiii iiii iiii iiii` |
| j | j target || `0000 10ii iiii iiii iiii iiii iiii iiii` |
| jal | jal target || `0000 11ii iiii iiii iiii iiii iiii iiii` |
| jr | jr $ra || `0001 11ss sss0 0000 0000 0000 0000 1000`	|
| addu.qb | addu.qb d, s, t || `0111 11ss ssst tttt dddd d000 0001 0000` |
| addu_s.qb | addu_s.qb d, s, t || `0111 11ss ssst tttt dddd d001 0001 0000` |
|sllv| sllv d, t, s | d = t << s | `0000 00ss ssst tttt dddd dxxx xx00 0100` |
