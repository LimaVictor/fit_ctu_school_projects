module reg_file ( input [4:0] a1 , a2 , a3,
                  input [31:0] wd3, //save
                  input clk, we3, reset,
                  output reg [31:0] rd1 , rd2);

	// Register file storage
	reg [31:0] rf[31:0];


	integer i;
	always@(*) begin
			rd1 = rf[a1];
			rd2 = rf[a2];
	end
	always@(posedge clk) begin
		if (reset) begin
			for (i = 0; i < 32 ; i = i + 1 ) begin
				rf[i] <=0;
			end
			end
		else if (we3 && a3!=0) begin
	 	 			rf[a3] <= wd3;
	end
	end
endmodule