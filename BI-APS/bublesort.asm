nop
start:
addi $a2, $zero, 1	# change=1
lw   $a1, 0x8		# store quantities of number in the "array"
addi $a1, $a1, -0x1
lw   $a0, 0xc 		# store address of the "array" to the register a0

jal sort
beq $zero, $zero, done

sort:
  beq  $a2, $zero, done_sort 	# while (change=1)
  addi $t6, $a0, 0
  addi $t1, $zero, 0   		# initialization instruction of for cycle: i=0, kde i=t1
  addi $a2, $zero, 0		#change = 0
  
inner_for:
  beq  $t1, $a1, sort		# 8 - quantities of number in "array"
  lw   $t2, 0x0($t6)    	# load value from the array to t2
  addi $t6, $t6, 0x4    	# increment offset and move to the other value in the array
  lw   $t3, 0x0($t6)    	# load value from the array to t3
  slt  $t5, $t2, $t3		# if t2 less then t3 -- set t5 to 1
  addi $t1, $t1, 1		#inc i cykl
  beq  $t5, 1, inner_for	# if (t2 < t3) skip, else continue
  
  sw   $t2, 0x0($t6)
  addi $t6, $t6, -0x4		#change offset
  sw   $t3, 0x0($t6)
  addi $t6, $t6, 0x4 
  addi $a2, $zero, 1
  beq  $zero, $zero, inner_for

done_sort: 
jr $ra

done:
nop
  
