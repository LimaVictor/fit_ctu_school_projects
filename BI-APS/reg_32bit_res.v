module reg_32bit_res ( input [31:0] datain,
                       input clk, reset, 
                       output reg [31:0] dataout);
  always@( posedge clk)
		if (reset) begin
		   dataout <= 0;
		   end
		else 
		   dataout <= datain;
endmodule